import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, InputGroup, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function ProductView() {

	const { user } = useContext(UserContext);

	
	const navigate = useNavigate(); //useHistory

	
	const { productId } = useParams();
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [stocks, setStocks] = useState(0);
	const [count, setCount] = useState(0);

	const subtractItem = (item) => {
		if(count > 0) {
			setCount(count -1)
			setStocks(stocks +1)
		}
	}
	
	const addItem = (item) => {
		if(count >= 0 && stocks !== 0) {
			setCount(count +1)
			setStocks(stocks -1)
		}
	}

	function orderProduct(e) {
		if(count !== 0) {
			fetch(`${process.env.REACT_APP_API_URL}/users/order`, {
				method: "PUT",
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					productId: productId,
					quantity: count
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);

				if(data === true) {
					Swal.fire({
						title: "Ordered",
						icon: "success",
						text: "You have sucessfully ordered this product."
					})

					navigate("/products")
				} else {
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					})
				}

			})
		} else {
			Swal.fire({
				title: "Quantity is Required",
				icon: "error",
				text: "Please add quantity"
			})
		}
	}

	

	

	useEffect(() => {

		 console.log(productId);

		 fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		 .then(res => res.json())
		 .then(data => {
		 	console.log(data)

		 	setName(data.name);
		 	setDescription(data.description);
		 	setPrice(data.price);
		 	setStocks(data.stocks);
		 })

	}, [productId])


	return (

			<Container>

				<Row className="mt-3 mb-3">
					<Col lg={{span: 6, offset:3}}>
						<Card>
						      <Card.Body className="text-left">
						        <Card.Title>{name}</Card.Title>
						        <Card.Subtitle className="mb-2">Description:</Card.Subtitle>
						        <Card.Text>
						          {description}
						        </Card.Text>
						        <Card.Title>Price:</Card.Title>
						        <Card.Text>PHP {price}</Card.Text>
						        <Card.Title>Stocks:</Card.Title>
						        <Card.Text>{stocks}</Card.Text>
						        <br/>
						        <Card.Title>Quantity</Card.Title>
						        <InputGroup className="mb-3 w-25">

						                <Button onClick={() => {
						       				subtractItem()}} className="secondary" variant="secondary" id="button-addon1">
						                  -
						                </Button>
						                <Form.Control
						        		  className="text-center"
						                  value={count}
						                  readOnly
						                  // onchange={item => setCount(item.target.value)}
						                  
						                />

						                <Button onClick={() => {
						       				addItem()}} className="secondary" variant="secondary" id="button-addon2">
						                  +
						                </Button>
						         </InputGroup>
						        
						       {

						       		(user.id !== null) ?
						       			<Button className="w-100 button-glow" onClick={e => orderProduct(e)}>Checkout</Button>
						       			:
						       			<Button className="danger" variant="danger" as={Link} to="/login" >Log in to Checkout</Button>



						       }
						      </Card.Body>
						</Card>

					</Col>
				</Row>


			</Container>
		)
}