import { useState, useEffect, useContext } from 'react';
import { Form, Button, ButtonGroup, Table, Container } from 'react-bootstrap';
import { Navigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import TableContent from '../components/TableContent';
import '../App.css';
import UserContext from '../UserContext';




export default function AdminDashboard() {

	const { user } = useContext(UserContext);

	const [products, setProducts] = useState([]);
	

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/allProducts`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setProducts(data.map(product => {
				return (
				<TableContent key={product._id} productProp={product}/>
				)
			}))
		})
	},[products])


	return (
			(user.isAdmin === true) ?

			<div className="text-center mt-3">
				<h1> Admin Dashboard</h1>
				<br/>
					<ButtonGroup>
				     <Button className="primary" variant="primary" as={ Link } to="/addproduct">Add New Product</Button>
				     <Button className="danger" variant="danger" as={ Link } to="/orderDetails">Show Users Order</Button>
				   </ButtonGroup>
				<br/>
				<br/>


				<Table striped bordered hover variant="dark">
				      <thead>
				        <tr>
				          <th className="w-25 p-1">Name</th>
				          <th className="w-50 p-1">Description</th>
				          <th className="w-auto p-1">Price</th>
				          <th className="w-auto p-1">Stocks</th>
				          <th className="w-auto p-1">Actions</th>
				        </tr>
				      </thead>
				      {products}
				      
				</Table>


			</div>
			:
			<Navigate to="/login"/>
		)
}