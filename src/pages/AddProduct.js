import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function AddProduct() {

	const { user } = useContext(UserContext);

	const navigate = useNavigate(); 

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [stocks, setStocks] = useState("");
	
	// State to determine wether the submit button is enabled or not.
	const [isActive, setIsActive] = useState(false);


	// Function to simulate user registration
	function addProduct(e) {
		e.preventDefault()

		// Clear the input fields and states
		setName("");
		setDescription("");
		setPrice("");
		setStocks("");

		fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`, {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				stocks: stocks
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

				if(data === true) {

					// Clear the input fields and states
					// Clear the input fields and states
					setName("");
					setDescription("");
					setPrice("");
					setStocks("");

					Swal.fire({
						title: "Added",
						icon: "success",
						text: "Product has been added successfully!"
					})
					navigate("/adminDashboard")
				} else {
					Swal.fire({
						title: "Something went wrong!",
						icon: "error",
						text: "Please check your input details "
					})
					navigate("/adminDashboard")
				}
			
		})

	}

	
	useEffect(() => {
		if(name !== "" && description !== "" && price !== "" && stocks !== "") {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [name, description, price, stocks])

	return (
			(user.isAdmin === true) ?
			
				<Form onSubmit={e => addProduct(e)}>


				      <Form.Group className="mb-3" controlId="productName">
				        <Form.Label>Product Name</Form.Label>
				        <Form.Control 
					        type="text" 
					        placeholder="Enter product name" 
					        value={name}
					        onChange={e => setName(e.target.value) }
					        required
				        />
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="productDescription">
				        <Form.Label>Description</Form.Label>
				        <Form.Control 
					        type="text" 
					        placeholder="Enter product description" 
					        value={description}
					        onChange={e => setDescription(e.target.value) }
					        required
				        />
				       </Form.Group>
				      

				      <Form.Group className="mb-3" controlId="productPrice">
				        <Form.Label>Price</Form.Label>
				        <Form.Control 
					        type="text" 
					        pattern="[0-9]+"
					        placeholder="Enter price" 
					        value={price}
					        onChange={e => setPrice(e.target.value) }
					        required
				        />
				      </Form.Group>

			            <Form.Group className="mb-3" controlId="productStocks">
			              <Form.Label>Stocks</Form.Label>
			              <Form.Control 
			      	        type="text" 
			      	        pattern="[0-9]+"
			      	        placeholder="Enter number of available stocks" 
			      	        value={stocks}
			      	        onChange={e => setStocks(e.target.value) }
			      	        required
			              />
			            </Form.Group>

				      

				    {
				    	isActive ?
				    		<Button className="button-glow" type="submit" id="submitBtn">
				    		  Submit
				    		</Button>
				    		:
				    		<Button className="button-glow" type="submit" disabled>
				    		  Submit
				    		</Button>
				    }
				    </Form>
				    :
				    <Navigate to="/login"/>
		)
}