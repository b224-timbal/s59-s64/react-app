import { useContext } from 'react';
import { Navbar, Nav, Container, Form, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import '../App.css';
import UserContext from '../UserContext';


export default function AppNavbar() {

	const { user } = useContext(UserContext);
	return (
				<Navbar className="navbar-color text-light" expand="lg">
				      <Container fluid>
				        <Navbar.Brand className="text-color" as={ Link } to="/">Online Store</Navbar.Brand>
				        <Navbar.Toggle aria-controls="navbarScroll" />
				        <Navbar.Collapse id="navbarScroll">
				          <Nav
				            className="ms-auto my-2 my-lg-0"
				            style={{ maxHeight: '100px' }}
				            navbarScroll
				          >
				            <Nav.Link className="text-color" as={ Link } to="/">Home</Nav.Link>
				            {
				            	(user.isAdmin)?
				            	<Nav.Link className="text-color" as={ Link } to="/adminDashboard">Dashboard</Nav.Link>
				            	:
				            	<>
				            		<Nav.Link className="text-color" as={ Link } to="/products">Products</Nav.Link>
				            		{/*<Nav.Link className="text-color" as={ Link } to="/myOrders">My Orders</Nav.Link>*/}
				            	</>

				            }

				            {
				            	(user.id !== null) ?
				            	<Nav.Link className="text-color" as={ Link } to="/logout">Logout</Nav.Link>
				            	
				            	:
				            	<>
				            		<Nav.Link className="text-color" as={ Link } to="/login">Login</Nav.Link>
				            		<Nav.Link className="text-color" as={ Link } to="/register">Register</Nav.Link>
				            	</>

				            }
				           
				          </Nav>
				          <Form className="d-flex">
				            <Form.Control
				              type="search"
				              placeholder="Search"
				              className="me-2 border-0"
				              aria-label="Search"
				            />
				            <Button className="button-glow">Search</Button>
				          </Form>
				        </Navbar.Collapse>
				      </Container>
				    </Navbar>

		)
}