import { useState, useEffect } from 'react';
import { Row, Col, Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function ProductCard({productProp}) {
	
	

	const { name, description, price, stocks, _id } = productProp;

	return (
				

				<Row className="mt-3 mb-3">
					<Col xs={12}>
						<Card className="cardHighlight p-3">
						      <Card.Body>
						        <Card.Title>{name}</Card.Title>
						        <Card.Subtitle className="mb-2">Description:</Card.Subtitle>
						        <Card.Text>
						          {description}
						        </Card.Text>
						        <Card.Title>Price:</Card.Title>
						        <Card.Text className="text-danger">PHP {price}</Card.Text>
						        <Card.Title>Stocks:</Card.Title>
						        <Card.Text>{stocks}</Card.Text>

						        <Button className="button-glow" as={ Link } to={`/products/${_id}`}>Details</Button>
						      </Card.Body>
						</Card>

					</Col>
				</Row>

			)
}